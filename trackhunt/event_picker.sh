#!/bin/bash

ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source $ATLAS_LOCAL_ROOT_BASE/user/atlasLocalSetup.sh
asetup Athena,21.0.62,here

echo ""
echo "Printing event number to start at"
for var in "$@"
do 
    echo "$var"
done
echo ""

dump-eformat-in-data.py --start-event $1 --number-of-events 100 /eos/atlas/atlastier0/rucio/data18_cos/physics_FTK/00346795/data18_cos.00346795.physics_FTK.merge.RAW/data18_cos.00346795.physics_FTK.merge.RAW._lb0558._SFO-ALL._0001.1 | grep "source_id: TDAQ_FTK, module=1" -A 75 > /afs/cern.ch/work/t/tholmes/FTK/trackhunt/bytestream_558_$1.txt

