import os

infile = "bytestream_558_grep.txt"

ftk_packets = []
with open(infile, "r") as f:
    ftk_packet = []
    adding = False
    for i, line in enumerate(f):
        if i%10000000 == 0: print "Processing line", i, "approximately event", i/286261

        if "0x007f0001" in line and "TDAQ_FTK" in line:
            adding = True
        elif "source_id" in line:
            if len(ftk_packet)>0:

                # check for tracks
                for j, word in enumerate(ftk_packet):
                    if "bda" in word.split()[1] and "data[0]" in word:
                        print "found track header on line", i+j-len(ftk_packet)+1
                        ftk_packets.append(ftk_packet)
                        print ftk_packet

                ftk_packet = []
            adding = False
        if adding:
            ftk_packet.append(line)

