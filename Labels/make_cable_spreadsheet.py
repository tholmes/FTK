#!/usr/bin/env python

import csv
import datetime

aux_layout = {}
df_layout = {}
atca_rack_layout = {}
vme_rack_layout = {}

#input_csv = "left_towers.csv"
input_csv = "right_towers.csv"

# Parse csv by hand to get an indexable object
parsed_lines = []
with open(input_csv, "r") as f:
    lines = csv.reader(f)
    for line in lines:
        items = []
        for item in line: items.append(item)
        parsed_lines += [items]

headers = parsed_lines[0]
rack_line = parsed_lines[22]

# Get AUX/AMB information out
for line in parsed_lines:
    if "SSB" in line[2]: continue
    if "sbc-ftk-rcc" in line[2]: continue
    if line[0].isdigit():
        slot = int(line[0])
        # Loop over columns with defined aux/amb
        for i in [1,2,9,10]:
            crate = int(''.join(c for c in headers[i] if c.isdigit()))
            tower_str = ''.join(c for c in line[i] if c.isdigit())
            if tower_str == '': continue
            aux_layout[(crate,slot)] = int(tower_str)

            # Add crate-rack correspondence if not already there
            rack = "Y."+rack_line[1].strip("rack ")+".A2"
            if i > 2: rack = "Y."+rack_line[9].strip("rack ")+".A2"
            if crate not in vme_rack_layout.keys(): vme_rack_layout[crate] = rack

# Get DF information out
for line in parsed_lines:
    if not ',' in line[5]: continue
    slot = int(line[4])
    # Loop over columns with defined DFs
    for i in [5, 6]:
        shelf = int(''.join(c for c in headers[i] if c.isdigit()))
        towers = [int(x) for x in line[i].split(',')]
        df_layout[(shelf,slot)] = towers

        # Add crate-rack correspondence if not already there
        rack = "Y."+rack_line[4].strip("rack ")+".A2"
        if shelf not in atca_rack_layout.keys(): atca_rack_layout[shelf] = rack

# For each DF, there are 2 towers; we need 2 cables to AUX (QSFP1, QSFP2) and one to SSB
# Each AUX has 1 cable to SSB

class Cable:

    def __init__(self, item_id=None, function=None, type_=None, diameter=None, length=None, userfield=None, startpoint=None, endpoint=None, datetime=None):
        self.ITEM_ID = item_id
        self.FUNCTION = function
        self.TYPE = type_
        self.DIAMETER = diameter
        self.LENGTH = length
        self.USERFIELD = userfield
        self.STARTPOINT_RACK_Y = startpoint
        self.ENDPOINT_RACK_Y = endpoint
        self.DATE_TIME = datetime
        self.DETECTOR = "FTK"
        self.SUBSYSTEM = "FTK"
        self.CROSS_SECTION = 0
        self.STARTPOINT_ROOM = "USA15"
        self.STARTPOINT_LEVEL = 2
        self.ENDPOINT_ROOM = "USA15"
        self.ENDPOINT_LEVEL = 2

        self.CONTACT = "Tova Holmes"

    def __str__(self):
        output_str = ""
        for category in categories:
            if hasattr(self, category):
                output_str += str(eval("self.%s"%category)) + ","
            else:
                output_str += ","
        return output_str

# Categories
categories = "ITEM_ID LPNAME  DETECTOR    FUNCTION    SUBSYSTEM   TYPE    DIAMETER    CROSS_SECTION   LENGTH  STANDARDFIELD   USERFIELD   STARTPOINT_LOCATION STARTPOINT_ROOM STARTPOINT_LEVEL    STARTPOINT_RACK_X   STARTPOINT_RACK_Y   STARTPOINT_SIDE  ENDPOINT_LOCATION   ENDPOINT_ROOM   ENDPOINT_LEVEL  ENDPOINT_RACK_X ENDPOINT_RACK_Y ENDPOINT_SIDE   TRAYS   POSITION_IN_TRAY    MANUFACTURE_REFERENCE   WEIGHT  BENDRADIUS  CABLE_DESCRIPTION   CABLE_CHARACTERISTIC_1  CABLE_CHARACTERISTIC_2  CABLE_CHARACTERISTIC_3  CABLE_CHARACTERISTIC_4  CABLE_CHARACTERISTIC_5  CABLE_CHARACTERISTIC_6  SOURCE_ID   DESTINATION_ID  STATUS  DOCUMENTATIONS_EDMS_REF DATE_TIME   ROUTINGPANEL    CONTACT CONNECTOR   START_POINT END_POINT   LABEL    SERIAL_NUMBER   START_POINT_VERIF   END_POINT_VERIF".split()

# starting ID
start_id = 0
def getID(label_dic):
    total_len = 0
    for key in label_dic:
        total_len += len(label_dic[key])
    return total_len

# set today's date
today = datetime.date.today().isoformat() #YYYY-MM-DD

labels = {"df-aux": [], "df-ssb": [], "aux-ssb": []}
for df in df_layout:
    for i_tower in xrange(2):

        # Find AUX to connect to
        tower = df_layout[df][i_tower]
        matched_aux = -1
        for aux in aux_layout:
            if aux_layout[aux] == tower: matched_aux = aux
        if matched_aux == -1: print "Couldn't find AUX connected to DF", df

        # Find SSB to connect to
        ssb_slot = (matched_aux[1]+6) - matched_aux[1]%5

        # Make cables that connect DF - AUX
        for i_cable in xrange(2):

            # Gather info
            i_rtm = i_tower*2+(i_cable+1)%2
            label_name = "\"DF-%02d-%02d to AUX-%02d-%02d\nRTM %d to QSFP %d\""%(df[0], df[1], matched_aux[0], matched_aux[1], i_rtm, i_cable+1)
            rack_startpoint = atca_rack_layout[df[0]]
            rack_endpoint = vme_rack_layout[matched_aux[0]]

            # Add cable to database
            cable = Cable(item_id=start_id+getID(labels), function="QSFP active cable", type_="Optical Cable", diameter=3, length=10,
                            userfield=label_name, startpoint=rack_startpoint, endpoint=rack_endpoint, datetime=today)
            labels["df-aux"] += [cable]

        # Make cables that connect DF - SSB
        if i_tower==0:
            label_name = "DF-%02d-%02d to SSB-%02d-%02d"%(df[0], df[1], matched_aux[0], ssb_slot)
            cable = Cable(item_id=start_id+getID(labels), function="QSFP active cable", type_="Optical Cable", diameter=3, length=10,
                            userfield=label_name, startpoint=rack_startpoint, endpoint=rack_endpoint, datetime=today)
            labels["df-ssb"] += [cable]

        # Make cables that connect AUX - SSB
        aux_type = "A"
        if i_tower>0: aux_type = "C"

        label_name = "\"AUX-%02d-%02d to SSB-%02d-%02d\n(AUX %s)\""%(matched_aux[0], matched_aux[1],matched_aux[0], ssb_slot, aux_type)
        rack_startpoint = vme_rack_layout[matched_aux[0]]

        cable = Cable(item_id=start_id+getID(labels), function="SFP cable", type_="Optical Cable", diameter=2, length=3,
                            userfield=label_name, startpoint=rack_startpoint, endpoint=rack_endpoint, datetime=today)
        labels["aux-ssb"] += [cable]

print ""
for category in categories:
    print category+",",
print ""
for ltype in labels:
    print "Cable type:", ltype
    for l in labels[ltype]:
        print l



